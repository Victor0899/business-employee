package com.everis.escuela.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.everis.escuela.models.*;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>
{
	
}