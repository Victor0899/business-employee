package com.everis.escuela.web;


import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.escuela.models.Employee;
import com.everis.escuela.service.EmployeeService;

@RestController
@RequestMapping("/api/business/v1")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping("/employees")
	public String createEmployee(@RequestBody Employee employee){
		
		employeeService.saveEmployee(employee);
		return "Se guardó exitosamente";
	}
}
