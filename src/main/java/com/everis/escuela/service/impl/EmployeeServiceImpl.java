package com.everis.escuela.service.impl;

import org.springframework.stereotype.Service;

import com.everis.escuela.models.*;
import com.everis.escuela.repository.*;
import com.everis.escuela.service.*;

@Service
public class EmployeeServiceImpl implements EmployeeService{
	
	private EmployeeRepository employeeRepository;

	@Override
	public void saveEmployee(Employee employee) {
		employeeRepository.save(employee);
		
	}
	
	
	
}
