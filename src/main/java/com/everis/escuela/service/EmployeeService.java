package com.everis.escuela.service;

import com.everis.escuela.models.Employee;

public interface EmployeeService
{
	public void saveEmployee(Employee employee);
}